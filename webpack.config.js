
module.exports = {
  entry: "./themes/vuejs/static/js/component.js",
  output: {
    filename: "themes/vuejs/static/js/bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  }
}

---
title: Early detection triples a patient's chances of survival from crc (colorectal cancer)
decription: Early detection triples a patient's chances of survival from crc (colorectal cancer)
date: 2015-11-07T00:00:00.000Z
video:
  - 'https://www.youtube.com/watch?v=60Rup8rEFjA'
---

#### Direction to complete test for Screening of Colorectal Cancer - FIT (Fecal Immunochemical Test)

1. Place supplied collection paper inside of the toilet bowl, on top of water underneath the seat.
2. Collect bowel movement onto the collection paper.
3. Unscrew lid of collection bottle. Using probe, scrape and collect stool sample making sure to cover the grooved portion completely.
4. After inserting the probe into the tube, snap the green cap tightly. Do not reopen.
5. **Return the sample to your doctor or laboratory in the envelope provided.**
------
Vietnamese:

Hướng dẫn:

5. **Xin quý vị vui lòng bỏ mẫu xét nghiệp vào phong bì (đi cùng) và mang trở lại văn phòng bác sĩ gia đình hoặc là cơ sở xét nghiệm.**



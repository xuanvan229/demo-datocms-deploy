---
title: About Van Lang IPA
decription: About Van Lang IPA
date: 2015-11-07T00:00:00.000Z
---

Van Lang IPA, an independent physician association founded by primary care physicians, is a physician led organization. We engage with high quality primary care physicians, specialists, hospital systems and ancillary services that share our mission to provide superior care through innovation, technology and collaboration.

At Van Lang IPA, our physicians value the confidence entrusted by our patients, their friends and family in selecting Van Lang IPA physicians as their primary care providers.  Our primary care doctors are experienced at collaboration, communication, and coordination with specialists and hospital systems to provide excellent care for chronic disease management.  Over 75% of our primary care physicians have Saturday clinic hours to improve our patients’ ability to access care.  


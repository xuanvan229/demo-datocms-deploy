---
title: Our leadership
decription: Our leadership
date: 2015-11-07T00:00:00.000Z
---

### IPA Leadership

|                   |                                       |
|:------------------|:--------------------------------------|
|Dac Vu, MD         |    President & CEO, Secretary         |
|Tram Ho, MD        |    Chief Financial Officer & Secretary|
|Nghia Nguyen, MD   |    Medical Director                   |
|Jerry Tsao, MD     |    Director of Medical Credentialing  |

### Our Management Team
|                   |                                                        |
|:------------------|:-------------------------------------------------------|
|Lan Pham, MPH, CPUR|    President & CEO                                     |
|Don Phan           |    Chief Financial Officer                             |
|Ruby Grainger      |    Provider Network Operations, Senior Manager         |
|Jeff Ngo           |    Provider Network Operations, Manager (Texas)        |
|Douglas Nguyen     |    Director of Information Services                    |
|Bree Kerlin, CPCS  |    Credentialing Manager                               |
|Chenae Hodge       |    Utilization Management & Quality Management, Manager|
|Leon Pham, PA      |    Manager of Medical Services & Case Manager          |   



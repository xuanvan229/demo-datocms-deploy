---
title: Advance Directives
decription: 'An advance directive is a legal document that tells your family, friends and healthcare professionals the care you would like to have if you become unable to make medical decisions. Through advance directives, you can make legally valid decisions about your future medical treatment.'
date: 2017-05-10T00:00:00.000Z
categories: Forms
---

* A **living will**, officially known in Texas as the Directive to Physicians and Family or Surrogates, describes the kind of medical treatments or life-sustaining treatments you would want if you were seriously or terminally ill. A living will should be signed, dated and witnessed by two people, preferably individuals who know you well but are not related to you and are not your potential heirs or your health care providers.

Download the form (English): 

[Download File](https://www.datocms-assets.com/2982/1504161863-directive-to-physicians-english.pdf)

Download the form (Spanish): 

[Download File](https://www.datocms-assets.com/2982/1504161924-directive-to-physicians-spanish.pdf)

* The **Out-of-Hospital Do Not Resuscitate (DNR) order** provides you with the right to withhold or withdraw cardiopulmonary resuscitation (CPR) or other treatments such as defibrillation and artificial ventilation.

Download the form (English): 

[Download File](https://www.datocms-assets.com/2982/1504161961-out-oh-hospital-do-not-resuscitate-english.pdf)

Download the form (Spanish)

[Download File](https://www.datocms-assets.com/2982/1504161993-out-oh-hospital-do-not-resuscitate-spanish.pdf)

By creating an advance directive, you are making your preferences about medical care known before you're faced with a serious injury or illness. This will spare your loved ones the stress of making decisions about your care while you are sick. Any person 18 years of age or older can prepare an advance directive.

In order to make your directive legally binding, you must sign it, or direct another to sign it, in the presence of two witnesses who must also sign the document.


---
title: Set Up Van Lang IPA As A Favorite In Athena
decription: How to setup Van Lang IPA as a favorite in Athena
date: 2015-11-07T00:00:00.000Z
categories: Claims
image: 'https://www.datocms-assets.com/2982/1504155799-athena-favourite-01.png?'
---

![Setup VLIPA as a favourite in Athena - Step 01](https://www.datocms-assets.com/2982/1504155799-athena-favourite-01.png?) 

 > Setup VLIPA as a favourite in Athena - Step 01

![Setup VLIPA as a favourite in Athena - Step 02](https://www.datocms-assets.com/2982/1504155823-athena-favourite-02.png?) 

 > Setup VLIPA as a favourite in Athena - Step 02

![Setup VLIPA as a favourite in Athena - Step 03](https://www.datocms-assets.com/2982/1504155831-athena-favourite-03.png?) 

 > Setup VLIPA as a favourite in Athena - Step 03

![Setup VLIPA as a favourite in Athena - Step 04](https://www.datocms-assets.com/2982/1504155841-athena-favourite-04.png?) 

 > Setup VLIPA as a favourite in Athena - Step 04


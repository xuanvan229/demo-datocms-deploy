---
title: How To Create A Claim Batch In eCW
decription: Instruction on how to create a claim batch in eClinical Works for Van Lang IPA Clearing House
date: 2017-11-07T00:00:00.000Z
categories: Claims
image: 'https://www.datocms-assets.com/2982/1503908913-ecw-claims-01.png?'
---

Instruction on how to create a claim batch in eClinical Works for Van Lang IPA Clearing House

 **STEP 1:**		Create a new payer (Van Lang IPA) in your eCW


|                                                           |
|:----------------------------------------------------------|
|VAN LANG IPA C/O MSO, INC. OF SOUTHERN CALIFORNIA          |
|2295 HUNTINGTON DRIVE, SUITE D                             |
|SAN MARINO, CA 91108                                       |
|CLAIMS PHONE: (626) 656-2370, OPTION 1                     |
|CLAIMS FAX: (866) 898-9765                                 |
|CLEARINGHOUSE PAYER ID: VLIPA                              |


**STEP 2:**
All Van Lang IPA patients from sep 1, 2015 and after – should have their insurance updated in ecw to reflect the newly added payer: Van Lang IPA in your eCW system.

* Update AmeriVantage Classic to Van Lang IPA – use same member id as amerigroup
* Update AmeriVantage Advantage to Van Lang IPA – use same member id as amerigroup
* Do not update amerigroup mmp patients – Van Lang IPA does not have this contract

All Van Lang IPA patients with dates of service before aug 30 2015 will be paid by Amerigroup

**STEP 3:**		CREATE YOUR CLAIM

**STEP 4:**		Generate your claims batch (SELECT VAN LANG IPA payer)

**STEP 5:**		Place CREATED BATCH in local designated file (on desktop, C drive, or network)

**STEP 6:**		Open new window and go to clearinghouse home page – www.freeclaims.com

**Note:** *Typically, you would go to eCW and select your clearinghouse to batch out your claim.  To batch out Van Lang IPA claims, you need to open a new window to go to the VLIPA clearinghouse home page to batch out.*

**STEP 7:**		Upload your CREATED BATCH --- it’s just like sending an email and attachment

![Step 1](https://www.datocms-assets.com/2982/1503908913-ecw-claims-01.png?) 

 > Step 1

![Step 2](https://www.datocms-assets.com/2982/1503908949-ecw-claims-02.png?) 

 > Step 2

![Step 3](https://www.datocms-assets.com/2982/1503908963-ecw-claims-03.png?) 

 > Step 3

![Step 4](https://www.datocms-assets.com/2982/1503908987-ecw-claims-04.png?) 

 > Step 4

![Step 5](https://www.datocms-assets.com/2982/1503909015-ecw-claims-05.png?) 

 > Step 5

![Step 6](https://www.datocms-assets.com/2982/1503909081-ecw-claims-06.png?) 

 > Step 6


---
title: How to set up paper claims in Athena
decription: How to setup Van Lang IPA as payer package to drop paper claims in Athena.
date: 2015-11-07T00:00:00.000Z
categories: Claims
image: 'https://www.datocms-assets.com/2982/1504151577-setup-athena-01.png?'
---

![Setup Athena - Step 01](https://www.datocms-assets.com/2982/1504151577-setup-athena-01.png?) 

 > Setup Athena - Step 01

![Setup Athena - Step 02](https://www.datocms-assets.com/2982/1504151593-setup-athena-02.png?) 

 > Setup Athena - Step 02

![Setup Athena - Step 03](https://www.datocms-assets.com/2982/1504151619-setup-athena-03.png?) 

 > Setup Athena - Step 03

![Setup Athena - Step 04](https://www.datocms-assets.com/2982/1504151636-setup-athena-04.png?) 

 > Setup Athena - Step 04

![Setup Athena - Step 05](https://www.datocms-assets.com/2982/1504151653-setup-athena-05.png?) 

 > Setup Athena - Step 05

![Setup Athena - Step 06](https://www.datocms-assets.com/2982/1504151666-setup-athena-06.png?) 

 > Setup Athena - Step 06


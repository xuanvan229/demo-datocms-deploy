
  var id_image;
  const menu = {
    items : [
    ]
  }
  const categories = {
    items : [
    ]
  }
  // create function return title for file markdown
  return_title = (article) =>{
    //check have categories
   if(article.category) {
    //check have video in categories
    if(article.video.length != 0){
      // check have image 
      if(check_image(article)){
          return {
            title: article.title,
            decription: article.decription,
            date: article.date,
            categories: return_categories(article.category.id),
            video: check_video(article),
            image: return_first_image(article)
          }
        } // if not have image 
        else {
            return {
              title: article.title,
              decription: article.decription,
              date: article.date,
              categories: return_categories(article.category.id),
              video: check_video(article),
            }
          }
    } // check haven't video in categories 
    else {
      // have image
        if(check_image(article)){
            return {
              title: article.title,
              decription: article.decription,
              date: article.date,
              categories: return_categories(article.category.id),
              image: return_first_image(article)
            }
          } // not have image  
          else {
            return {
              title: article.title,
              decription: article.decription,
              date: article.date,
              categories: return_categories(article.category.id),
            }
          }
        }
    } // check haven't categroeis 
    else {
      // check have video in haven't categories
      if(article.video.length != 0){
        // have image 
        if(check_image(article)){
            return {
              title: article.title,
              decription: article.decription,
              date: article.date,
              video: check_video(article),
              image: return_first_image(article)
            }
          } // not have image 
          else {
            return {
              title: article.title,
              decription: article.decription,
              date: article.date,
              video: check_video(article),
            }
          }         
      } // check haven't video in haven't categories 
      else {
        // have image
        if(check_image(article)){
            return {
              title: article.title,
              decription: article.decription,
              date: article.date,
              image: return_first_image(article)
            }
          } // not have image  
          else {
            return {
              title: article.title,
              decription: article.decription,
              date: article.date,
            }
        }
      }
    }
  }
  // return categories 
  return_categories = (id) =>{
    for (var i=0; i<categories.items.length;i++){
      if(id === categories.items[i].id){
        return categories.items[i].title
      }
    }
  }
  // check video
  check_video = (article) => {
    if(article.video) {
      var video = [];
      for (var  i = 0; i<article.video.length; i++) {
          video.push(article.video[i].video.url)
      }
      return video
    }
  }
  // check have image on content
  check_image = (article) =>{
    var check = false
    for(var i = 0; i<article.content.length;i++){
      if(article.content[i].imagegallery){
        check = true
        id_image = i
      } else  {
        check = false
      }
    }
    return check
  }
  return_first_image = (article) =>{
    return article.content[id_image].imagegallery[0].url()
  }
  return_items_data = (content) =>{
    var text = "";
    if (content.contenttext) {
      text = content.contenttext + "\n\n";
    }
    var file = "";
    if(content.file) {
      file = "[Download File]("+content.file.url()+")\n\n";
    }
    var gallery = "";
    if (content.imagegallery) {
    content.imagegallery.forEach(image => {
      gallery = gallery +"!["+image.alt+"]("+image.url()+") \n\n > "+image.title+"\n\n";
    })
  }
    return (text+file+gallery)
  }
  return_data = (article) => {
    var data = "";
    for ( var i=0; i<article.content.length;i++){
      data = data + return_items_data(article.content[i])
    }
    return data
  }
  module.exports = (dato, root, i18n) => {

  root.directory("content", (articlesDir) => {

    // iterate over all the available locales...
    i18n.availableLocales.forEach((locale) => {

      // switch to the nth locale
      i18n.withLocale(locale, () => {

        // iterate over the "Blog post" records...
        dato.categories.forEach((article) => {
          var items_categories = {id : article.id,  title: article.title}
          categories.items.push(items_categories)
        })
         // create menu parent
        dato.listmenus.forEach((article) => {
          if(!article.parent){
            var linkrm = article.titleuser.replace("/","")
            var links  = linkrm.split(' ').join('-')
            links = links.toLowerCase()
            var titles = {id: article.id, title: article.title, link: "/"+links, user: article.titleuser, submenu: []}
               menu.items.push(titles)
          }
        });
        // create menu children
        dato.listmenus.forEach((article) => {
          if(article.parent){
            var linkrm = article.titleuser.replace("/","")
            var links  = linkrm.split(' ').join('-')
            links = links.toLowerCase()
            var titles = {id: article.id, title: article.titleuser, link: "/"+links, user: article.titleuser, parentid: article.parent.id, submenu: []}
            for(var i = 0 ; i< menu.items.length ; i++){
              if(article.parent.id === menu.items[i].id ){
                menu.items[i].submenu.push(titles)
              }
            }
          }
        });
        root.createDataFile("data/foobar.json", 'json', menu)
        root.createDataFile("data/categories.json", 'json', categories)

        // add item for menu
        dato.pages.forEach((article) => {
            var nameparent;

          if(article.menuitem){
            // check on parent menu
            for (var i = 0; i< menu.items.length ; i++) {
              if(article.menuitem.id === menu.items[i].id){
                nameparent = menu.items[i].user;
                var links = nameparent.replace("/", "")
                links = links.split(' ').join('-')
                links = links.toLowerCase()
                var items = {title: article.title, link: '/'+links+'/'+article.slug+`.${locale}`}
                if(article.showinmenu){
                   menu.items[i].submenu.push(items)
                }
              } else { // if not in parent menu, check on children menu
                for(var j= 0 ; j< menu.items[i].submenu.length; j++){
                  if(article.menuitem.id === menu.items[i].submenu[j].id){
                    nameparent = menu.items[i].submenu[j].user                  
                  var links = nameparent.replace("/", "")
                  links = links.split(' ').join('-')
                  links = links.toLowerCase()
                    var items = {title: article.title, link: '/'+links+'/'+article.slug+`.${locale}`}
                    if(article.showinmenu){
                      menu.items[i].submenu[j].submenu.push(items)                      
                    }
                  }
                }
              }
            }
            // create file md filter name 
              articlesDir.createPost(
                `${nameparent}/${article.slug}.${locale}.md`, "yaml", {
                  frontmatter: return_title(article),
                  content: return_data(article)
                }
              );
          } else {
              articlesDir.createPost(
                `/${article.slug}.${locale}.md`, "yaml", {
                  frontmatter: return_title(article),
                  content: return_data(article)
                }
              );
          }
        });
      });
    });
  });    
}
